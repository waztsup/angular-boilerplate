import { ComponentsModule } from './components/components.module';
import { PipesModule } from './pipes/pipes.module';
import { NgModule } from "@angular/core";
import { DirectivesModule } from './directives/directives.module';

const modules: any[] = [
    ComponentsModule,
    DirectivesModule,
    PipesModule,
];

@NgModule({
    imports: modules,
    exports: modules
})
export class SharedModule { }
