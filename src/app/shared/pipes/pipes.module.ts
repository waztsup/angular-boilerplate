import { NgModule } from "@angular/core";

const pipes: any[] = [];

@NgModule({
    declarations: pipes,
    exports: pipes,
})
export class PipesModule { }
