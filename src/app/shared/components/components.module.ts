import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

const components: any[] = [];

@NgModule({
    declarations: components,
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: components
})
export class ComponentsModule { }
