import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RestService {

    private readonly serverLink = environment.serverLink;
    private options = {
        headers: new HttpHeaders({})
    };

    constructor(private http: HttpClient) { }

    get(endpoint: string, options = this.options) {
        return this.http.get<any>(`${this.serverLink}/${endpoint}`, options);
    }

    post(endpoint: string, item: any, options = this.options) {
        return this.http.post<any>(`${this.serverLink}/${endpoint}`, item, options);
    }

    put(endpoint: string, id: string | number, item: any, options = this.options) {
        return this.http.put<any>(`${this.serverLink}/${endpoint}/${id}`, item, options);
    }

    delete(endpoint: string, id: string | number, options = this.options) {
        return this.http.delete<any>(`${this.serverLink}/${endpoint}/${id}`, options);
    }
}