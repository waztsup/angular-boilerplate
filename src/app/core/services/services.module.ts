import { RestService } from './rest.service';
import { NgModule } from "@angular/core";

@NgModule({
    providers: [
        RestService
    ]
})
export class ServicesModule { }
